"""Tests standard tap features using the built-in SDK tests library."""

import datetime
import os

from singer_sdk.testing import get_tap_test_class

from tap_gsmtasks.tap import TapGSMtasks


SAMPLE_CONFIG = {
    "start_date": datetime.datetime.combine(datetime.date.today(), datetime.time.min,datetime.timezone.utc).strftime("%Y-%m-%d"),
    'auth_token': os.environ.get('TOKEN'),
}


# Run standard built-in tap tests from the SDK:
TestTapGSMtasks = get_tap_test_class(
    tap_class=TapGSMtasks,
    config=SAMPLE_CONFIG
)


# TODO: Create additional tests as appropriate for your tap.
