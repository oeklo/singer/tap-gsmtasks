from singer_sdk.authenticators import APIKeyAuthenticator
from singer_sdk.streams import Stream as RESTStreamBase


class TokenHeaderAuthenticator(APIKeyAuthenticator):
    """Implements bearer token authentication for REST Streams.

    This Authenticator implements Bearer Token authentication. The token
    is a text string, included in the request header and prefixed with
    'Bearer '. The token will be merged with HTTP headers on the stream.
    """

    def __init__(self, stream: RESTStreamBase, token: str) -> None:
        """Create a new authenticator.

        Args:
            stream: The stream instance to use with this authenticator.
            token: Authentication token.
        """
        super().__init__(stream=stream, key='Authorization', value=f"Token {token}")

    @classmethod
    def create_for_stream(
        cls,
        stream: RESTStreamBase,
        token: str
    ) -> 'TokenHeaderAuthenticator':
        return cls(stream=stream, token=token)
