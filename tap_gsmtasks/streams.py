"""Stream type classes for tap-gsmtasks."""

import datetime as dt
from collections.abc import Iterable
from pathlib import Path
from typing import Any
from urllib.parse import ParseResult

import requests

from tap_gsmtasks.client import GSMtasksStream

SCHEMAS_DIR = Path(__file__).parent / Path("./schemas")


class TaskStream(GSMtasksStream):
    name = 'task'
    path = '/tasks/'
    primary_keys = ['id']
    replication_key = 'updated_at'
    schema_filepath = SCHEMAS_DIR / "taskserializerv2.json"
    ordering = 'updated_at'
    replication_param = 'updated_at__gte'


class TaskMetaStream(GSMtasksStream):
    name = 'task_metadata'
    path = '/task_metadatas/'
    primary_keys = ['id']
    schema_filepath = SCHEMAS_DIR / "taskmetadata.json"
    ordering = 'last_updated_at'
    replication_key = 'last_updated_at'
    replication_param = 'updated_at__gte'

    def post_process(self, row: dict, context: dict | None = None) -> dict | None:
        return {
            **row,
            'last_updated_at': last_updated_at(row),
        }


DATE_FIELDS = (
    "last_unassigned_at",
    "last_assigned_at",
    "last_accepted_at",
    "last_transit_at",
    "last_active_at",
    "last_completed_at",
    "last_failed_at",
    "last_cancelled_at"
)


def last_updated_at(row: dict) -> str | None:
    dates = (dt.datetime.fromisoformat(row[field]) for field in DATE_FIELDS if field in row and row[field])
    if not dates:
        return None
    date_max = max(dates)
    return date_max.isoformat()


class UserStream(GSMtasksStream):
    name = 'user'
    path = '/users/'
    primary_keys = ['id']
    schema_filepath = SCHEMAS_DIR / 'readableuser.json'
