"""REST client handling, including GSMtasksStream base class."""

import datetime as dt
import logging
from collections.abc import Iterable
from pathlib import Path
from typing import Any
from urllib.parse import ParseResult, parse_qs

from singer_sdk.authenticators import APIKeyAuthenticator
from singer_sdk.pagination import HeaderLinkPaginator
from singer_sdk.streams import RESTStream

SCHEMAS_DIR = Path(__file__).parent / Path("./schemas")

API_VERSION = '2.4.13'

logger = logging.getLogger(__name__)


class GSMtasksStream(RESTStream[ParseResult]):
    """GSMtasks stream class."""

    url_base = "https://api.gsmtasks.com"

    def get_new_paginator(self) -> HeaderLinkPaginator:
        return HeaderLinkPaginator()

    @property
    def authenticator(self) -> APIKeyAuthenticator:
        """Return a new authenticator object.

        Returns:
            An authenticator instance.
        """
        return APIKeyAuthenticator.create_for_stream(
            self,
            key='Authorization',
            value='Token ' + self.config.get("auth_token", ""),
            location='header',
        )

    @property
    def http_headers(self) -> dict:
        """Return the http headers needed.

        Returns:
            A dictionary of HTTP headers.
        """
        headers = {
            'Accept': f'application/json; version={API_VERSION}',
        }
        if 'timezone' in self.config:
            headers['Accept-Timezone'] = self.config['timezone']
        if "user_agent" in self.config:
            headers["User-Agent"] = self.config["user_agent"]

        return headers

    def get_url_params(self, context: dict | None, next_page_token: ParseResult | None) -> dict[str, Any]:
        if next_page_token:
            return parse_qs(next_page_token.query)
        else:
            stream_type = type(self)

            params: dict[str, str | Iterable[str]] = {}
            if hasattr(stream_type, 'ordering'):
                params['ordering'] = stream_type.ordering
            if hasattr(stream_type, 'replication_param'):
                replication_param = getattr(stream_type, 'replication_param')

                if start_date := self.get_starting_timestamp(context):
                    params[replication_param] = start_date.isoformat()
                else:
                    start_date_str = self.config['start_date']
                    if 'timezone' in self.config:
                        timezone = self.config['timezone']
                    else:
                        timezone = 'UTC'

                    import zoneinfo
                    completed_at = dt.datetime.combine(dt.date.fromisoformat(start_date_str), dt.time.min, tzinfo=zoneinfo.ZoneInfo(timezone))
                    params[replication_param] = completed_at.isoformat()

            self.logger.info('params: %s ', params)
            return params
