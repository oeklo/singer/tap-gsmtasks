from singer_sdk import Tap
from singer_sdk import typing as th


class TapGSMtasks(Tap):
    name = "tap-gsmtasks"

    config_jsonschema = th.PropertiesList(
        th.Property(
            "auth_token",
            th.StringType,
            required=True,
            secret=True,  # Flag config as protected.
            description="The token to authenticate against the API service",
        ),
        th.Property(
            "timezone",
            th.StringType,
            description='Timezone sent as Accept-Timezone header to GSMtasks API',
        ),
        th.Property(
            "start_date",
            th.DateTimeType,
            description="The earliest record date to sync",
        ),
        th.Property(
            'stream_maps',
            th.ObjectType()
        ),
        th.Property(
            'stream_map_config',
            th.ObjectType()
        ),
    ).to_dict()

    def discover_streams(self) -> list:
        from tap_gsmtasks import streams
        """Return a list of discovered streams.

        Returns:
            A list of discovered streams.
        """
        return [
            streams.TaskStream(self),
            streams.TaskMetaStream(self),
            streams.UserStream(self),
        ]


if __name__ == "__main__":
    TapGSMtasks.cli()
